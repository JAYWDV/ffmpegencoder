#include "stdafx.h"

int main(int argc, char **argv)
{
	
	AVFormatContext *ifmt_ctx = NULL, *ofmt_ctx = NULL;
	AVPacket pkt;
	const char *in_filename, *out_filename;	
	int ret;

	in_filename = "C:\\input.ts";

	if ((ret = avformat_open_input(&ifmt_ctx, in_filename, 0, 0)) < 0)
	{
		fprintf(stderr, "Could not open input file '%s'", in_filename);
		
	}
	FILE* fp = fopen("C:\\output.txt", "w");
	if (fp)
	{
		TCHAR log[20];
		_stprintf(log, L"Number of stream %d", ifmt_ctx->nb_streams);
		fwrite(log,  2,20, fp);
		fclose(fp);
	}	
	return 0;
}
