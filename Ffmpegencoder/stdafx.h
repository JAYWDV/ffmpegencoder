// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#define	 __STDC_CONSTANT_MACROS
#define __STDC_FORMAT_MACROS
#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <stdlib.h>
#include <string.h>

extern "C"
{
	#include "libavformat/avformat.h"
}

// TODO: reference additional headers your program requires here
